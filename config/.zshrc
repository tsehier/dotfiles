# Config for composer global exec bin, exemple Laravel Envoy
export PATH=$PATH:~/.config/composer/vendor/bin
# Aliases
alias dcup="docker-compose up -d"
alias dcdown="docker-compose down -v"
